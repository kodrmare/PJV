package intelliJ.thedrake.ui;

import intelliJ.thedrake.game.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TheDrakeApplication extends Application {

  public static void main(String[] args) {
	launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
	GameState state = createTestGame();
	
	BoardView boardView = new BoardView(state);
	Scene scene = new Scene(boardView);
	stage.setScene(scene);
	stage.setTitle("The Drake");
	stage.show();
  }

  private Board createTestBoard() {
	StandardDrakeSetup setup = new StandardDrakeSetup();
	Board board = new Board(
			4,
			new CapturedTroops(),
			new TroopTile(new TilePosition("a1"), new Troop(setup.MONK, PlayingSide.BLUE)),
			new TroopTile(new TilePosition("b1"), new Troop(setup.DRAKE, PlayingSide.BLUE)),
			new TroopTile(new TilePosition("a2"), new Troop(setup.SPEARMAN, PlayingSide.BLUE)),
			new TroopTile(new TilePosition("c2"), new Troop(setup.CLUBMAN, PlayingSide.BLUE)),
			new TroopTile(new TilePosition("a4"), new Troop(setup.ARCHER, PlayingSide.ORANGE, TroopFace.BACK)),
			new TroopTile(new TilePosition("b4"), new Troop(setup.DRAKE, PlayingSide.ORANGE, TroopFace.BACK)),
			new TroopTile(new TilePosition("c3"), new Troop(setup.SWORDSMAN, PlayingSide.ORANGE)));
	return board;
  }

  private GameState createTestGame() {
	Board board = createTestBoard();
	StandardDrakeSetup setup = new StandardDrakeSetup();
	return new MiddleGameState(
			board,
			new BasicTroopStacks(setup.CLUBMAN),
			new BothLeadersPlaced(new TilePosition("b1"), new TilePosition("b4")),
			PlayingSide.BLUE);
  }
}
