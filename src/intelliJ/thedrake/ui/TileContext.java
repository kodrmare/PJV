package intelliJ.thedrake.ui;


import intelliJ.thedrake.game.Move;

public interface TileContext {
  public void tileSelected(TileView view);
  public void execute(Move move);
}
