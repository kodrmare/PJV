package intelliJ.thedrake.media;

import intelliJ.thedrake.game.Board;

public interface BoardMedia<T> {
	public T putBoard(Board board);
}
