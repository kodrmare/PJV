
package intelliJ.thedrake.media;

import intelliJ.thedrake.game.MiddleGameState;
import intelliJ.thedrake.game.PlacingGuardsGameState;
import intelliJ.thedrake.game.PlacingLeadersGameState;
import intelliJ.thedrake.game.VictoryGameState;

public interface GameStateMedia<T> {
	public T putPlacingLeadersGameState(PlacingLeadersGameState state);
	public T putPlacingGuardsGameState(PlacingGuardsGameState state);
	public T putMiddleGameState(MiddleGameState state);
	public T putFinishedGameState(VictoryGameState state);
}

