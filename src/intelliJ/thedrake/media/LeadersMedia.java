package intelliJ.thedrake.media;

import intelliJ.thedrake.game.BothLeadersPlaced;
import intelliJ.thedrake.game.NoLeadersPlaced;
import intelliJ.thedrake.game.OneLeaderPlaced;

public interface LeadersMedia<T> {
	public T putNoLeadersPlaced(NoLeadersPlaced leaders);
	public T putOneLeaderPlaced(OneLeaderPlaced leaders);
	public T putBothLeadersPlaced(BothLeadersPlaced leaders);
}
