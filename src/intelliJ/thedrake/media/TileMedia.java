package intelliJ.thedrake.media;

import intelliJ.thedrake.game.EmptyTile;
import intelliJ.thedrake.game.TroopTile;

public interface TileMedia<T> {
	public T putTroopTile(TroopTile tile);	
	public T putEmptyTile(EmptyTile tile);
}
