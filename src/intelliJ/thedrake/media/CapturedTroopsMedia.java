package intelliJ.thedrake.media;

import intelliJ.thedrake.game.CapturedTroops;

public interface CapturedTroopsMedia<T> {
	public T putCapturedTroops(CapturedTroops captured);
}
