package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.CapturedTroops;
import intelliJ.thedrake.game.PlayingSide;
import intelliJ.thedrake.game.TroopInfo;
import intelliJ.thedrake.media.CapturedTroopsMedia;
import intelliJ.thedrake.media.PrintMedia;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

public class CapturedTroopsPlainTextMedia extends PrintMedia implements CapturedTroopsMedia<Void> {

    public CapturedTroopsPlainTextMedia(OutputStream stream) {
		super(stream);
    }
    @Override
    public Void putCapturedTroops(CapturedTroops captured) {
        boolean first = true;
        PrintWriter w = writer();

        List<TroopInfo> tmpCaptured = captured.troops(PlayingSide.BLUE);
        w.printf("Captured %s: %d%n", PlayingSide.BLUE, tmpCaptured.size());

        for (TroopInfo blueTroop: tmpCaptured){
            w.printf("%s%n", blueTroop.name());
        }


        tmpCaptured = captured.troops(PlayingSide.ORANGE);
        w.printf("Captured %s: %d", PlayingSide.ORANGE, tmpCaptured.size());

        if(tmpCaptured.size() > 0) {
            w.println();
        }

        for (TroopInfo orangeTroop: tmpCaptured){
            if(!first) w.println();
            w.printf("%s", orangeTroop.name());
            first = false;
        }

        return null;
    }

}
