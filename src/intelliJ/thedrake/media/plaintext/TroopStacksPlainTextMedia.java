package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.BasicTroopStacks;
import intelliJ.thedrake.game.PlayingSide;
import intelliJ.thedrake.game.TroopInfo;
import intelliJ.thedrake.media.PrintMedia;
import intelliJ.thedrake.media.TroopStacksMedia;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

public class TroopStacksPlainTextMedia extends PrintMedia implements TroopStacksMedia<Void> {
    
    public TroopStacksPlainTextMedia(OutputStream stream){
        super(stream);
    }

    @Override
    public Void putBasicTroopStacks(BasicTroopStacks stacks) {
        PrintWriter w = writer();
        List<TroopInfo> blueStack = stacks.troops(PlayingSide.BLUE);
        List<TroopInfo> orangeStack = stacks.troops(PlayingSide.ORANGE);
        
        w.print("BLUE stack:");
        for(TroopInfo blueTroop: blueStack){
            w.printf(" %s",blueTroop.name());
        }
        w.println();
        
        w.print("ORANGE stack:");
        for(TroopInfo orangeTroop: orangeStack){
            w.printf(" %s", orangeTroop.name());
        }
        w.println();
        
        
        return null;
    }
    
}