package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.BothLeadersPlaced;
import intelliJ.thedrake.game.NoLeadersPlaced;
import intelliJ.thedrake.game.OneLeaderPlaced;
import intelliJ.thedrake.game.PlayingSide;
import intelliJ.thedrake.media.LeadersMedia;
import intelliJ.thedrake.media.PrintMedia;

import java.io.OutputStream;
import java.io.PrintWriter;

public class LeadersPlainTextMedia extends PrintMedia implements LeadersMedia<Void> {
    
    public LeadersPlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putNoLeadersPlaced(NoLeadersPlaced leaders) {
        PrintWriter w = writer();
        w.println("NL");
        
        return null;
    }

    @Override
    public Void putOneLeaderPlaced(OneLeaderPlaced leaders) {
        PrintWriter w = writer();
        w.print("OL ");
        if(leaders.isPlaced(PlayingSide.BLUE))
            w.printf("%s", leaders.position(PlayingSide.BLUE).toString());
        else
            w.printf("X %s", leaders.position(PlayingSide.ORANGE).toString());
        w.println();
        return null;
    }

    @Override
    public Void putBothLeadersPlaced(BothLeadersPlaced leaders) {
        PrintWriter w = writer();
        w.printf("BL %s %s%n",
                leaders.position(PlayingSide.BLUE).toString(),
                leaders.position(PlayingSide.ORANGE).toString());
        
        return null;
    }
    
}