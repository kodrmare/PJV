package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.*;

import java.io.BufferedReader;
import java.io.IOException;

public class GameStateFromPlainText {
	private final BufferedReader reader;
	private final TroopStacksFromPlainText stacksFromPlainText;
	private final LeadersFromPlainText leadersFromPlainText;
	private final BoardFromPlainText boardFromPlainText;
	
	public GameStateFromPlainText(TheDrakeSetup setup, BufferedReader reader) {
		this.reader = reader;
		this.stacksFromPlainText = new TroopStacksFromPlainText(setup, reader);
		this.leadersFromPlainText = new LeadersFromPlainText(setup, reader);
		this.boardFromPlainText = new BoardFromPlainText(setup, reader);
	}
	
	public GameState readGameState() throws IOException {
		String type = reader.readLine();
		int guards = Integer.parseInt(reader.readLine());
		PlayingSide sideOnTurn = PlayingSide.valueOf(reader.readLine());		 
		
		TroopStacks stacks = stacksFromPlainText.readTroopStacks();
		Leaders leaders = leadersFromPlainText.readLeaders();
		Board board = boardFromPlainText.readBoard();

		switch (type) {
			case "LEADERS":
				return new PlacingLeadersGameState(board, stacks, leaders, sideOnTurn);
			case "GUARDS":
				return new PlacingGuardsGameState(board, stacks, (BothLeadersPlaced) leaders, sideOnTurn, guards);
			case "MIDDLE":
				return new MiddleGameState(board, stacks, (BothLeadersPlaced) leaders, sideOnTurn);
			case "VICTORY":
				return new VictoryGameState(board, stacks, (OneLeaderPlaced) leaders, sideOnTurn);
		}
		
		throw new IOException("Invalid file format");
	}
}
