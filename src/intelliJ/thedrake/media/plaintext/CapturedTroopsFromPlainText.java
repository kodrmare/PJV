package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.CapturedTroops;
import intelliJ.thedrake.game.PlayingSide;
import intelliJ.thedrake.game.TheDrakeSetup;
import intelliJ.thedrake.game.TroopInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CapturedTroopsFromPlainText {
	private final TheDrakeSetup setup;
	private final BufferedReader reader;
	
	public CapturedTroopsFromPlainText(TheDrakeSetup setup, BufferedReader reader) {
		this.setup = setup;
		this.reader = reader;
	}
	
	public CapturedTroops readTroops() throws IOException {
		return new CapturedTroops(readList(PlayingSide.BLUE), readList(PlayingSide.ORANGE));
	}
	
	private List<TroopInfo> readList(PlayingSide side) throws IOException {
		String line = reader.readLine();
		int count;
		if(!line.startsWith("Captured " + side.toString() + ":")) {
			throw new IOException("Invalid file format");
		}
		
		String[] fields = line.split(":");
		
		count = Integer.parseInt(fields[1].trim());
		List<TroopInfo> captured = new ArrayList<>();

		for(int i = 0; i < count; i++) {
			line = reader.readLine();
			captured.add(setup.infoByName(line));
		}
		
		return captured;
	}
}
