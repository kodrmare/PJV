package intelliJ.thedrake.media.plaintext;

import intelliJ.thedrake.game.EmptyTile;
import intelliJ.thedrake.game.Troop;
import intelliJ.thedrake.game.TroopTile;
import intelliJ.thedrake.media.PrintMedia;
import intelliJ.thedrake.media.TileMedia;

import java.io.OutputStream;
import java.io.PrintWriter;

public class TilePlainTextMedia extends PrintMedia implements TileMedia<Void>{
    
    public TilePlainTextMedia(OutputStream stream) {
		super(stream);
    }
    
    @Override
    public Void putTroopTile(TroopTile tile) {
        PrintWriter w = writer();
        Troop troop = tile.troop();
        w.printf("%s %s %s", troop.info().name(), troop.side(), troop.face());
        
        return null;
    }

    @Override
    public Void putEmptyTile(EmptyTile tile) {
        PrintWriter w = writer();
        
        w.print("empty");
        w.flush();
        
        return null;
    }

    
}
