package intelliJ.thedrake.media;

import intelliJ.thedrake.game.BasicTroopStacks;

public interface TroopStacksMedia<T> {
	public T putBasicTroopStacks(BasicTroopStacks stacks);
}
